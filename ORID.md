Objective: 

1. Code Review : review each other's homework with my teammates , Incorporate feedback from teammates to modify my code, such as rewriting the parking boy as an inheritance mode.
1. Strategy Pattern Session:  Use a real life example to illustrate the application of the Strategy Pattern.  
1. Refactoring: Learn what refactoring is, why, and how to do it with word-frequency-refactor example.  

Reflective:  Fruitful and Challenging.

Interpretive:  During this day of study, I learned a lot about refactoring, and I met some difficulties in the process of practicing because there are many types of refactoring and I don't know all of them.

Decisional:  I'm going to learn more about refactoring! I will also actively seek the help of teachers and classmates when I have problem.
