import java.util.*;
import java.util.stream.Collectors;


public class WordFrequencyGame {

    public static final String SEPARATOR = "\\s+";
    public static final String DELIMITER = "\n";

    public String getResult(String inputStr) {
        try {
            Map<String, Integer> wordCountMap = transformToMap(inputStr);

            List<Map.Entry<String, Integer>> wordCountList = sortedToList(wordCountMap);

            return returnResult(wordCountList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static List<Map.Entry<String, Integer>> sortedToList(Map<String, Integer> wordCountMap) {
        return wordCountMap.entrySet().stream()
                .sorted((entry1, entry2) -> entry2.getValue() - entry1.getValue())
                .collect(Collectors.toList());
    }

    private String returnResult(List<Map.Entry<String, Integer>> wordCountList) {
        StringJoiner joiner = new StringJoiner(DELIMITER);
        wordCountList.forEach(entry -> joiner.add(entry.getKey() + " " + entry.getValue()));
        return joiner.toString();
    }

    private static Map<String, Integer> transformToMap(String inputStr) {
        String[] splitArray = inputStr.split(SEPARATOR);
        Map<String, Integer> wordCountMap = new HashMap<>();
        Arrays.stream(splitArray).forEach(word -> wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1));
        return wordCountMap;
    }

}
